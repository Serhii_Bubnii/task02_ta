package com.bubnii.view;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/people?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    private static Connection connection;
    private static DriverManagerDataSource driverManagerDataSource;

    private ConnectionUtil(){
    }

    public static Connection getConnectionUtil(){
        try {
            Class.forName(JDBC_DRIVER);
            if (connection == null){
                connection = DriverManager.getConnection(DB_URL,USER,PASSWORD);
            }
        } catch (ClassNotFoundException | SQLException e) {
            throw new ExceptionInInitializerError(e);
        }
        return connection;
    }

    public static DriverManagerDataSource getDataSource(){
        driverManagerDataSource.setUsername(USER);
        driverManagerDataSource.setPassword(PASSWORD);
        driverManagerDataSource.setUrl(DB_URL);
        driverManagerDataSource.setDriverClassName(JDBC_DRIVER);
        return driverManagerDataSource;
    }
}
