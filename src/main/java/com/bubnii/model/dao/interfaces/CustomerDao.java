package com.bubnii.model.dao.interfaces;

import com.bubnii.model.entity.Customer;

import java.util.List;

public interface CustomerDao extends CrudDao<Customer> {

    List<Customer> findAllByName(String name);

}
