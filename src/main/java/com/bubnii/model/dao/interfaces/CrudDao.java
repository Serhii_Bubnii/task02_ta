package com.bubnii.model.dao.interfaces;

import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {

    Optional<T> findById(Integer id);

    Integer save(T model);

    void update(T model);

    void delete(Integer id);

    List<T> findAll();
}
