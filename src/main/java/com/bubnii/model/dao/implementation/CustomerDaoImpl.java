package com.bubnii.model.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.bubnii.model.dao.interfaces.CustomerDao;
import com.bubnii.model.entity.Customer;

public class CustomerDaoImpl implements CustomerDao {

    private final String SQL_DELETE = "DELETE FROM CUSTOMERS WHERE ID = ?";
    private final String SQL_UPDATE = "UPDATE CUSTOMERS SET NAME = ?, AGE = ?, SALARY = ? WHERE ID = ?";
    private final String SQL_SELECT_ALL = "SELECT * FROM CUSTOMERS";
    private final String SQL_SELECT_FIND_BY_ID = "SELECT * FROM CUSTOMERS WHERE ID = ?";
    private final String SQL_INSERT = "INSERT INTO CUSTOMERS(NAME, AGE, SALARY) values (?,?,?)";
    private final String SQL_SELECT_FIND_BY_NAME = "SELECT * FROM CUSTOMERS WHERE NAME = ?";
    private Connection connection;

    public CustomerDaoImpl(Connection connection) {
        this.connection = connection;
    }

    public Optional<Customer> findById(Integer id) {
        if (id < 0){
            throw new IllegalArgumentException();
        }
        try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_FIND_BY_ID)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("NAME");
                Integer age = resultSet.getInt("AGE");
                Double salary = resultSet.getDouble("SALARY");

                return Optional.of(new Customer(id, name, age, salary));
            }
            return Optional.empty();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public Integer save(Customer model) {
        int candidateId = 0;
        try (PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            connection.setAutoCommit(false);
            ResultSet resultSet;
            statement.setString(1, model.getName());
            statement.setInt(2, model.getAge());
            statement.setDouble(3, model.getSalary());
            int rowAffected = statement.executeUpdate();
            if (rowAffected == 1) {
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    candidateId = resultSet.getInt(1);
                }
            }
            connection.commit();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return candidateId;
    }

    public void update(Customer model) {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            connection.setAutoCommit(false);
            statement.setString(1, model.getName());
            statement.setInt(2, model.getAge());
            statement.setDouble(3, model.getSalary());
            statement.setInt(4, model.getId());

            int rowAffected = statement.executeUpdate();
            System.out.println(String.format("Row affected %d", rowAffected));
            connection.commit();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public void delete(Integer id) {
        try (PreparedStatement statement = connection.prepareStatement(SQL_DELETE)) {
            connection.setAutoCommit(false);
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {

            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                Integer age = resultSet.getInt("AGE");
                Double salary = resultSet.getDouble("SALARY");

                Customer customer = new Customer(id, name, age, salary);

                customers.add(customer);
            }
            return customers;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public List<Customer> findAllByName(String name) {
        List<Customer> customers = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_FIND_BY_NAME)) {
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                Integer age = resultSet.getInt("AGE");
                Double salary = resultSet.getDouble("SALARY");

                Customer customer = new Customer(id, name, age, salary);

                customers.add(customer);
            }
            return customers;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}
