package com.bubnii.service;

import com.bubnii.model.dao.interfaces.CustomerDao;
import com.bubnii.model.entity.Customer;

import java.util.List;
import java.util.Optional;

public class CustomerServiceTemplate {

    private CustomerDao customerDao;

    public CustomerServiceTemplate(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public Optional<Customer> findById(Integer id) {
        return customerDao.findById(id);
    }

    public Integer save(Customer model) {
        return customerDao.save(model);
    }

    public void update(Customer model) {
        customerDao.update(model);
    }

    public void delete(Integer id) {
        customerDao.delete(id);
    }

    public List<Customer> findAll() {
        return customerDao.findAll();
    }

    public List<Customer> findAllByName(String name){
        return customerDao.findAllByName(name);
    }
}
