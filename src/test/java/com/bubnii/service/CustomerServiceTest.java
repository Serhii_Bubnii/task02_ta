package com.bubnii.service;

import com.bubnii.model.dao.interfaces.CustomerDao;
import com.bubnii.model.entity.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CustomerServiceTest {

    private static Logger LOGGER = LogManager.getLogger(CustomerServiceTest.class);
    @Mock
    private CustomerDao customerDao;
    @Mock
    private CustomerService customerService;

    @BeforeSuite
    public void setUp() {
        LOGGER.info("create dependencies");
        customerDao = Mockito.mock(CustomerDao.class);
        customerService = new CustomerService(customerDao);
    }

    @AfterSuite
    public void tearDown() {
        LOGGER.info("clean dependencies");
        customerDao = null;
        customerService = null;
    }

    @Test
    public void testFindById() {
        LOGGER.info("test find customer by id");
        when(customerService.findById(1))
                .thenReturn(java.util.Optional.of(createTestEntity(1, "Tom", 23, 560d)));
        when(customerService.findById(45))
                .thenReturn(java.util.Optional.empty());
        when(customerService.findById(-1)).thenThrow(IllegalArgumentException.class);
    }

    @Test
    public void testSave() {
        LOGGER.info("test insert customer in DB");
        customerService.save(createTestEntity(1,"Bob", 22, 500d));
        Mockito.verify(customerDao).save(createTestEntity(1,"Bob", 22, 500d));
    }

    @Test
    public void testUpdate() {
        LOGGER.info("test update customer in DB");
        customerService.update(createTestEntity(4, "Lola", 27, 1800d));
        Mockito.verify(customerDao).update(createTestEntity(4, "Lola", 27, 1800d));
    }

    @Test
    public void testDelete() {
        LOGGER.info("test delete customer from DB");
        customerService.delete(1);
        Mockito.verify(customerDao).delete(1);
    }

    @Test
    public void testFindAll() {
        LOGGER.info("test find all customers from DB");
        when(customerDao.findAll()).thenReturn(Arrays.asList(
                createTestEntity(1, "Tom", 23, 900d),
                createTestEntity(2, "Anna", 19, 500d),
                createTestEntity(3, "Bob", 35, 3600d),
                createTestEntity(4, "Lola", 27, 1800d)));
        List<Customer> actual = customerService.findAll();
        assertEquals(4, actual.size());
        assertEquals(actual.get(1), createTestEntity(2, "Anna", 19, 500d));
        Mockito.verify(customerDao).findAll();
    }

    @Test
    public void testFindAllByName() {
        LOGGER.info("test find all customers by name");
        when(customerService.findAllByName("Alan"))
                .thenReturn(Arrays.asList(
                        createTestEntity(1, "Alan", 23, 900d),
                        createTestEntity(9, "Alan", 27, 1800d)));

        when(customerService.findAllByName("Yan"))
                .thenReturn(null);

        List<Customer> actual = customerService.findAllByName("Alan");
        assertEquals("Alan", actual.get(1).getName());

        Mockito.verify(customerDao).findAllByName("Alan");
    }

    private Customer createTestEntity(Integer id, String name, Integer age, Double salary) {
        Customer customer = new Customer();
        customer.setId(id);
        customer.setName(name);
        customer.setAge(age);
        customer.setSalary(salary);
        return customer;
    }
}